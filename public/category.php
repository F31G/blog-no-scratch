
<link rel="stylesheet" href="/../public/style.css">

<?php

// j'inclue les fonctions
require __DIR__ . '/../lib/functions.php';

// incrustation du header
getPart('header');

// incrustation du menu nav
getPart('menu');

?>

<h2>Nos catégories</h2>

<div class=categ>
<?php

//Initialisation myphp + GIT ignore OK
require __DIR__ . '/../logmyphp.php';
$reponse = $bdd->query('SELECT * FROM `categories` ');

while ($donnees = $reponse->fetch()){
    echo '<p>' . 
    '<a href="cat.php?id=' . $donnees['category'].'">' . $donnees['category'] . 
    '</a></p>';
}

?>

</div>
<!-- <a href="category.php?id='.$donnees['category'].'"> -->

<?php

// inclure le footer
getPart('footer');

?>
