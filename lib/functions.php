<?php

// function getContent(){
//     $page = $_GET['page'];
//     if(!isset($page)){           include __DIR__.'/../pages/header.php';}
//     elseif($page == 'home'){     include __DIR__.'/../pages/footer.php';} 
// }


/* 
vous ajouterez ici les fonctions qui vous sont utiles dans le site,
je vous ai créé la première qui est pour le moment incomplète et qui devra contenir
la logique pour choisir la page à charger
*/


function getContent(){
	if(!isset($_GET['page'])){
		include __DIR__.'/../parts/header.php';
	} else {
		if($_GET['page'] == "footer"){
			include __DIR__.'/../parts/footer.php';
		}
		if($_GET['page'] == "menu"){
			include __DIR__.'/../parts/menu.php';
		}
		if($_GET['page'] == "logmyphp"){
			include __DIR__.'/../logmyphp.php';
		}
	}
}

function getPart($name){
	include __DIR__ . '/../parts/'. $name . '.php';
}

function getUserData(){
	$us_json = file_get_contents(__DIR__.'/../data/user.json');
	$user = json_decode($us_json, true);
	return $user;
}

?>